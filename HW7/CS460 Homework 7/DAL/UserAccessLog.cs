namespace CS460_Homework_7.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserAccessLog
    {
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string SearchString { get; set; }

        [Required]
        [StringLength(1024)]
        public string AgentString { get; set; }

        public DateTime TimeStamp { get; set; }

        [Required]
        [StringLength(64)]
        public string IPAddress { get; set; }

        [Required]
        [StringLength(5)]
        public string NSFW { get; set; }
    }
}

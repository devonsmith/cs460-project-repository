﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CS460_Homework_7.DAL
{
    public partial class ActivityLogContext : DbContext
    {
        public ActivityLogContext() : base("name=ActivityLog")
        {

        }
        public virtual DbSet<UserAccessLog> UserAccessLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Search String
            modelBuilder.Entity<UserAccessLog>().Property(e => e.SearchString).IsUnicode(false);
            // User Agent String
            modelBuilder.Entity<UserAccessLog>().Property(e => e.AgentString).IsUnicode(false);
            // Time Stamp
            modelBuilder.Entity<UserAccessLog>().Property(e => e.TimeStamp);
            // IP address
            modelBuilder.Entity<UserAccessLog>().Property(e => e.IPAddress).IsUnicode(false);
            // Safe-search indicator
            modelBuilder.Entity<UserAccessLog>().Property(e => e.NSFW).IsUnicode(false);
        }
    }
}
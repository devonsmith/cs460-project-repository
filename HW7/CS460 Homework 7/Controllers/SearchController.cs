﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using System.IO;
using CS460_Homework_7.Models;
using System.Diagnostics;
using System.Collections;
using CS460_Homework_7.DAL;

namespace CS460_Homework_7.Controllers
{
    public class SearchController : Controller
    {
        private ActivityLogContext database = new ActivityLogContext();

        // GET: Search JSON Object
        [HttpGet]
        public JsonResult Search(string safeSearch)
        {
            #region "Giphy Interaction"

            /*********** Things we get from the user ****************************/
            // The rating is determined by if the user is using safe search or not
            // quick bool creation because this will be used multiple times and a
            // conditional check against a bool is fast and a comparison against a
            // string is slow.
            bool nsfwSearch = (safeSearch == "nsfw");
            // set the rating of the search to get back from the Giphy API.
            string rating = (nsfwSearch) ? "r" : "g";


            /************ Things that are dynamically created ****************************************/
            // The URL for the content
            string uri = "https://api.giphy.com/v1/gifs/search?api_key="
                       + System.Web.Configuration.WebConfigurationManager.AppSettings["giphyAPIKey"]
                       // The user's query
                       + "&q=" + Request.QueryString["q"]
                       // The rating requested by the user.
                       + "&rating=" + rating
                       // Always use English.
                       + "&lang=en";

            // Create a WebRequest
            WebRequest dataRequest = WebRequest.Create(uri);
            Stream dataStream = dataRequest.GetResponse().GetResponseStream();
            // Get the gallery data

            // This example will get the data as a object graph, and while this is nice, 
            // the graph is slower to parse and not as useful. I will instead use a class
            // to deserialize the Json data to.
            //var galleryData = new System.Web.Script.Serialization.JavaScriptSerializer()
            //                      .DeserializeObject(new StreamReader(dataStream)
            //                      .ReadToEnd());

            // Deserialize to the root class from GiphyImage.cs.
            var galleryData = new System.Web.Script.Serialization.JavaScriptSerializer()
                                  .Deserialize<RootObject>(new StreamReader(dataStream)
                                  .ReadToEnd());

            // Close
            dataStream.Close();
            // Create a list of images that will be passed to the client.   
            List<GiphyImage> images = new List<GiphyImage>();

            for (int i = 0; i < 25; ++i)
            {
                // create a new GiphyImage object
                GiphyImage giphyImage = new GiphyImage();
                // the data object from the list of images inside the galleryData
                Datum data = galleryData.data[i];
                // get the preview image URL from the datum
                giphyImage.preview = data.images.downsized_medium.url;
                // get the link to the Giphy page for the image from the datum.
                giphyImage.url = data.url;
                // get video alternatives for the gifs
                // MP4 for browsers that support it.
                giphyImage.mp4 = data.images.fixed_width.mp4;
                // Webp for browsers that support that.
                giphyImage.webp = data.images.fixed_width.webp;
                // add the image data to the list.
                images.Add(giphyImage);
            }
            #endregion
            #region "Data Logging"
            /********************* Logging of user search data ****************************/
            // create the log item.
            var log = database.UserAccessLogs.Create();
            // What did the user search for?
            log.SearchString = Request.QueryString["q"];
            // The user agent string.
            log.AgentString = Request.UserAgent;
            // The time of the transaction
            log.TimeStamp = DateTime.Now;
            // The IP address of the request.
            log.IPAddress = Request.UserHostAddress;
            // Was the user looking for nsfw content?
            log.NSFW = (nsfwSearch) ? "True" : "False";
            // Add the information to the database.
            database.UserAccessLogs.Add(log);
            // save the changes to the database.
            database.SaveChanges();

            #endregion

            // Return the Json request
            return Json(images, JsonRequestBehavior.AllowGet);
        }
    }
}
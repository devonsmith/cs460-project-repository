﻿using CS460_Homework_7.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CS460_Homework_7.Controllers
{
    public class HomeController : Controller
    {
        private ActivityLogContext database = new ActivityLogContext();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SearchLog()
        {

            return View(database.UserAccessLogs.ToList());
        }
    }
}
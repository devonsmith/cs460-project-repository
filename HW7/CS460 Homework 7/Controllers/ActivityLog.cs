namespace CS460_Homework_7.Controllers
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using CS460_Homework_7.DAL;

    public partial class ActivityLog : DbContext
    {
        public ActivityLog()
            : base("name=ActivityLog")
        {
        }

        public virtual DbSet<UserAccessLog> UserAccessLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}

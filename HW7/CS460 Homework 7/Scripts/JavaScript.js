﻿// Action for the search button being clicked.
$("#searchButton").click(function () {
    var urlString = ($("#nsfw").prop("checked") == true) ? "/Search/nsfw?q=" : "/Search?q=";
    if ($("#labelStickers").hasClass("label-success")) {
        urlString += "&s=stickers";
    }
    $.ajax(
        {
            type: "GET",
            dataType: "json",
            url: urlString + $("#searchGiphy").val(),
            success: function (data) { displayData(data); },
            error: function () { ajaxError(); }
        }
    );
});


// Actions for the NSFW check box to make your selection more obvious.
$("#nsfw").click(function () {
    if ($("#nsfw").prop("checked") == true) {
        $("#nsfwPanel").addClass("nsfw");
    }
    else {
        $("#nsfwPanel").removeClass("nsfw");
    }
});

// Actions for the NSFW check box to make your selection more obvious.
$("#video").click(function () {
    if ($("#video").prop("checked") == true) {
        $("#videoPanel").addClass("video");
    }
    else {
        $("#videoPanel").removeClass("video");
    }
});

// Display the data returned from the search.
function displayData(data) {
    $("#searchContainer").empty();
    if ($("#video").prop("checked") == true) {
        $.each(data, function (i, image) {
            $("#searchContainer").append(
                "<div class='img-box col-lg-4'>"
                + "<video loop autoplay>"
                + "<source src='"
                + image["mp4"]
                + "' type='video/mp4'>"
                + "<source src='"
                + image["webp"]
                + "' type='video/webp'>"
                + "</video>"
                + "</div>"
            );
        });
    }
    else {
        $.each(data, function (i, image) {
            $("#searchContainer").append(
                "<div class='img-box col-lg-4'><a href='"
                + image["url"]
                + "' target='_new'><img class='img-responsive' src='"
                + image["preview"]
                + "' /></a>"
                + "</div>"
            );
        });
    }
}

// if there is an error, return it.
function ajaxError() {
    alert("Warning: Unable to perform search. Please try again later.");
}
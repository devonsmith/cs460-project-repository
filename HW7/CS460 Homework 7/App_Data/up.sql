﻿CREATE TABLE UserAccessLogs(
ID int IDENTITY(0,1) PRIMARY KEY,
SearchString NVARCHAR(255) NOT NULL,
AgentString NVARCHAR(1024) NOT NULL,
TimeStamp DATETIME NOT NULL,
IPAddress NVARCHAR(64) NOT NULL,
NSFW NVARCHAR(5) NOT NULL
);

-- create some sample data
INSERT INTO  UserAccessLogs(SearchString, AgentString, TimeStamp, IPAddress, NSFW)
VALUES ( 'Test Search', 
		 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299',
		 '2017-12-01 00:00:00',
		 '0.0.0.0', 
		 'False');

-- Test query for sample data
SELECT * FROM UserAccessLogs;
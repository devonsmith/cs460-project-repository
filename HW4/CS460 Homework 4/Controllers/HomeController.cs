﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CS460_Homework_4.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// ActionResult returns default view for home page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Default action return from page 1.
        /// </summary>
        /// <returns></returns>
        public ActionResult Page1()
        {
            string email = Request.QueryString["email"];
            string password = Request.QueryString["password"];

            ViewBag.Email = email;
            ViewBag.Password = password;

            return View();
        }

        /// <summary>
        /// Default ActionResult for Page 2
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Page2()
        {
            return View();
        }
        /// <summary>
        /// Action result for a return post response from Page 2.
        /// </summary>
        /// <param name="form">FormCollection containing data from user form.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Page2(FormCollection form)
        {
            ViewBag.Results = "true";
            if (form["firstName"] != "" && form["lastName"] != "")
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(form["firstName"].ToLower().Substring(0, 1));
                sb.Append(form["lastName"].ToLower());
                sb.Append(DateTime.Now.Year.ToString().Substring(2, 2));
                ViewBag.UserName = sb.ToString();
            }
            else
            {
                ViewBag.UserName = null;
            }
            return View();
        }
        /// <summary>
        /// This ActionResult will return the default view for page 3.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Page3()
        {
            return View();
        }
        /// <summary>
        /// Performs a loan calculation based on user input and returns that information to the
        /// view.
        /// </summary>
        /// <param name="loanAmount">The total loan amount</param>
        /// <param name="downPayment">The down payment made when getting the loan</param>
        /// <param name="interestRate">The interest rate of the loan.</param>
        /// <param name="term">The term of the loan</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Page3(string loanAmount, string downPayment, string interestRate, string term)
        {
            // Simple try-catch block. This will catch any errors from user input.
            try
            {
                // remove characters that a user might enter into the input boxes
                interestRate = interestRate.Replace("%", "");
                loanAmount = loanAmount.Replace("$", "");
                downPayment = downPayment.Replace("$", "");
                // since this is being used in the calculation more than once, we'll
                // calculate it here and have it for reuse.
                double rate = Convert.ToDouble(interestRate) / 1200;
                /*
                 * This interest rate formula came from: 
                 * https://en.wikipedia.org/wiki/Equated_monthly_installment
                 * The values have been condensed to simplest form for this calculation
                 */
                double payment = (
                    (Convert.ToDouble(loanAmount) - Convert.ToDouble(downPayment))
                    * (rate)
                    * Math.Pow((1 + rate), Convert.ToDouble(term))
                    / (Math.Pow((1 + rate), Convert.ToDouble(term)) - 1));
                /*Compose the ViewBag*/
                // Convert the loan amount and down payment to a decimal and output is as currency
                ViewBag.LoanAmount = Convert.ToDecimal(loanAmount).ToString("C");
                ViewBag.DownPayment = Convert.ToDecimal(downPayment).ToString("C");
                // just output the trimed interest rate to the ViewBag.
                ViewBag.InterestRate = interestRate;
                ViewBag.Term = term;
                // Convert the Monthly payment to currency.
                ViewBag.MonthlyPayment = ((decimal)payment).ToString("C");
                // Make sure the ErrorMessage is empty so the error feedback isn't triggered on the
                // page.
                ViewBag.ErrorMessage = "";
            }
            // Going to catch a generic exception and print out the message. I could collect different exceptions
            // for different errors, but in this case almost all errors are going to be a product of user input 
            // errors when converting strings to doubles.
            catch (Exception e)
            {
                // If there is an error put a non-empty string into the viewbag for output to the user.
                ViewBag.ErrorMessage = e.Message;
            }


            return View();
        }

    }  
}
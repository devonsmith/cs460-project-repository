## CS460 Project Repository

This is a code repository for CS460 at Western Oregon University. This repository contains all of the projects that were created in that class and their code. You can find more information at the GitHub pages for this class which can be found [here](http://devonsmith.github.io).

### Projects Contained in this Repository

#### Homework 1

This project was to create a basic HTML website using CSS/HTML and Bootstrap. I was to learn the basics of Bootstrap and how to build a website using that framework.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW1.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw1)
* [Demo](https://devonsmith.github.io/cs460/hw1/demo)

#### Homework 2

This project is an extension of the skills learned in Homework 1. This project was the creation of a Javascript application that can take user input from multiple input types. In this case a text box, checkbox, and button. This application is a postfix calulator which is also the subject of Homework 3.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW2.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw2)
* [Demo](https://devonsmith.github.io/cs460/hw2/demo)

#### Homework 3

This project is to take a project from the Data Structures class at Western Oregon University and reimplement it in C#. The assignment is to create a Postfix calculator that has the same fundamental function as the Java version.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW3.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw3)
* [Binary Download](https://bitbucket.org/devonsmith7696/cs460-project-repository/downloads/Postfix%20Calculator.exe)


#### Homework 4

This project was used to learn .NET MVC 5 by creating a multi-page web application.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW4.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw4)

#### Homework 5

The objective of this project was to create a .NET MVC 5 web application using a simple database.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW5.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw5)

#### Homework 6

This Create a .NET MVC 5 web application using a complex pre-existing database. For this project I used the Micorosft AdventureWorks sample database.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW6.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw6)

#### Homework 7

The objective of this project was to create a .NET MVC 5 web application that uses a REST API. For this project I used the Gphy Search API.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW7.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw7)


#### Homework 8

A sample project that represents the skills required to pass the programming final. This is the programming final from the previous year.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW9.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw9)

#### Homework 9

This project contains no code in this repository. This homework assigment was just getting the Homework 8 assignment deployed to Azure. This is a requirement of the programming final at the end of the term.

* [Assignment Overview](http://www.wou.edu/~morses/classes/cs46x/assignments/HW9.html)
* [Github Pages Journal](https://devonsmith.github.io/cs460/hw9)

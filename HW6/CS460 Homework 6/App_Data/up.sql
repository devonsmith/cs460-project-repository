﻿RESTORE FILELISTONLY  
FROM DISK = 
'C:\Users\Devon Smith\Desktop\AdventureWorks2014.bak'
GO 

RESTORE DATABASE AdventureWorks2014
FROM DISK = 
	'C:\Users\Devon Smith\Desktop\AdventureWorks2014.bak'
WITH MOVE 'AdventureWorks2014_Data' 
    TO 'C:\Users\Devon Smith\Documents\Visual Studio 2017\Projects\CS460 Homework 6\CS460 Homework 6\App_Data\AdventureWorks2014.mdf',
MOVE 'AdventureWorks2014_Log' 
    TO 'C:\Users\Devon Smith\Documents\Visual Studio 2017\Projects\CS460 Homework 6\CS460 Homework 6\App_Data\AdventureWorks2014.ldf'
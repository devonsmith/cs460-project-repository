﻿SELECT P.Name AS Product, PC.Name AS Category, PSC.Name AS Subcategory,
	PM.Name AS Model, P.ListPrice
	FROM Production.Product AS P
	FULL JOIN Production.ProductModel AS PM ON PM.ProductModelID = P.ProductModelID
	FULL JOIN Production.ProductSubcategory AS PSC ON PSC.ProductSubcategoryID = P.ProductSubcategoryID
	JOIN Production.ProductCategory AS PC ON PC.ProductCategoryID = PSC.ProductCategoryID
	WHERE PC.Name = 'Bikes'
ORDER BY PC.Name, PSC.Name;



Select * from Production.ProductSubcategory;
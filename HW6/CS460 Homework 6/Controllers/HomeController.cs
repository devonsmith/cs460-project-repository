﻿using CS460_Homework_6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CS460_Homework_6.Controllers
{
    public class HomeController : Controller
    {
        private AdventureWorksEntityDataModel database = new AdventureWorksEntityDataModel();

        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            var features = database.Products.Where(
                    p => p.ProductSubcategory
                    .ProductCategory.Name == "Bikes").OrderByDescending(p => p.ListPrice);
            var featuredItems = features.ToList().Take(3);
            return View(featuredItems);
        }
        // GET: Bicycles
        [HttpGet]
        public ActionResult Bicycles(string id)
        {
            if (id == null || id == "All")
            {
                ViewBag.Category = "All Bicycles";
                var bikes = database.Products.Where(
                    p => p.ProductSubcategory
                    .ProductCategory.Name == "Bikes")
                    .OrderBy(p => p.ProductSubcategory.Name);
                return View(bikes.ToList());
            }
            else
            {
                string identifier = Convert.ToString(id);
                ViewBag.Category = identifier + " Bikes";
                var bikes = database.Products.Where(
                    p => p.ProductSubcategory.Name == id + " Bikes");
                return View(bikes.ToList());
            }
        }
        // GET: Clothing
        [HttpGet]
        public ActionResult Clothing(string id)
        {
            if (id == null || id == "All")
            {
                ViewBag.Category = "Clothing";
                var accessories = database.Products.Where(p => p.ProductSubcategory.ProductCategory.Name == "Clothing");
                return View(accessories.ToList());
            }
            else
            {
                string identifier = Convert.ToString(id);
                ViewBag.Category = id;
                var accessories = database.Products.Where(s => s.ProductSubcategory.Name == id);
                return View(accessories.ToList());
            }
        }
        // GET: Components
        [HttpGet]
        public ActionResult Components(string id)
        {
            if (id == null || id == "All")
            {
                ViewBag.Category = "Components";
                var accessories = database.Products.Where(p => p.ProductSubcategory.ProductCategory.Name == "Components");
                return View(accessories.ToList());
            }
            else
            {
                string identifier = Convert.ToString(id);
                ViewBag.Category = id;
                var accessories = database.Products.Where(s => s.ProductSubcategory.Name == id);
                return View(accessories.ToList());
            }
            
        }
        // GET: Accessories
        [HttpGet]
        public ActionResult Accessories(string id)
        {
            if (id == null || id == "All")
            {
                ViewBag.Category = "Accessories";
                var accessories = database.Products.Where(p => p.ProductSubcategory.ProductCategory.Name == "Accessories");
                return View(accessories.ToList());
            }
            else
            {
                string identifier = Convert.ToString(id);
                ViewBag.Category = id;
                var accessories = database.Products.Where(s => s.ProductSubcategory.Name == id);
                return View(accessories.ToList());
            }
        }

        // GET: Product
        [HttpGet]
        public ActionResult Product(string id)
        {
            int identifier = (id == null) ? 0 : Convert.ToInt32(id);
            var item = database.Products.Where(p => p.ProductID == identifier);
            return View(item.ToList());
        }
        [HttpGet]
        public ActionResult ReviewProduct(int id)
        {
            var product = database.Products.Where(p => p.ProductID == id).FirstOrDefault();
            byte[] image = product.ProductProductPhotoes.FirstOrDefault().ProductPhoto.LargePhoto;
            ViewBag.Image = "data:image/png;base64," + Convert.ToBase64String(image, 0, image.Length);
            ViewBag.Product = product.Name;
            ProductReview review = new ProductReview();
            review.ProductID = product.ProductID;
            return View(review);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReviewProduct([Bind(Include = "ProductReviewID, ProductID, ReviewerName, ReviewDate, EmailAddress, Rating, Comments, CommentsModifiedDate, Product")] ProductReview review, int id)
        {
            var product = database.Products.Where(p => p.ProductID == id).FirstOrDefault();
            byte[] image = product.ProductProductPhotoes.FirstOrDefault().ProductPhoto.LargePhoto;
            ViewBag.Image = "data:image/png;base64," + Convert.ToBase64String(image, 0, image.Length);
            ViewBag.Product = product.Name;
            review.ProductID = id;
            review.ReviewDate = DateTime.Now;
            review.ModifiedDate = review.ReviewDate;

            if (ModelState.IsValid)
            {
                database.ProductReviews.Add(review);
                database.SaveChanges();
                TempData["Success"] = "Your review has been submitted!";
                return Redirect("/Home/Product/" + id.ToString());
            }
            return View(review);
        }
    }
};
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS460_Homework_5.Models;
using CS460_Homework_5.DAL;

namespace CS460_Homework_5.Controllers
{
    /// <summary>
    /// Controller for all pages in the Home route. This is the only route in this project.
    /// </summary>
    public class HomeController : Controller
    {
        private RequestContext database = new RequestContext();
        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include ="ID, ODL, FullName, DOB, Street, City, USState, Zip, County, DateStamp")] Request request)
        { 
            // Check to see if the model state was good, if it was time to do some work.
            if (ModelState.IsValid)
            {
                // Add the new request to the database.
                database.Requests.Add(request);
                database.SaveChanges();
                // TempDataDIctionary TempData can be used to move information into a redirect
                //  the ViewData and ViewBag cannot.
                TempData["Success"] = "Your request has been logged.";
                return RedirectToAction("Index");
            }
            // Return the index view with the request
            return View(request);
        }
        // GET: Requests
        public ActionResult Requests()
        {
            // return a view with the DBSet as a list that can be parsed.
            return View(database.Requests.ToList());
        }
    }
}
﻿-- Create a table for storing the change of address requests
CREATE TABLE Requests(
    -- unique ID for the change of the request. This is not identified by the user that requested the transfer.
	-- This will auto-increment as new requests are added to the database.
	ID int IDENTITY(0,1) PRIMARY KEY,
	-- The ODL number identifying the person that wants the address change.
	ODL int NOT NULL,
	-- The name of the person requesting the change of address
	FullName VARCHAR(255) NOT NULL,
	-- The date of birth of the person requesting the change.
	DOB DATE NOT NULL,
	-- Street address
	Street VARCHAR(255) NOT NULL,
	-- City
	City VARCHAR(255) NOT NULL,
	-- State 
	USState VARCHAR(2) NOT NULL,
	-- zip
	Zip int NOT NULL,
	-- County
	County VARCHAR(100) NOT NULL,
	DateStamp DATE NOT NULL

);

-- Add Some Testing Values to the database
-- 1 
INSERT INTO Requests (ODL, FullName, DOB, Street, City, USState, Zip, County, DateStamp) 
	VALUES (1000000, 
			'John Smith', 
			'1970-02-26', 
			'123 NE Rainwater Drive', 
			'Albany', 
			'OR', 
			97321, 
			'Linn',
			'2017-06-15'
);
-- 2
INSERT INTO Requests (ODL, FullName, DOB, Street, City, USState, Zip, County, DateStamp) 
	VALUES (1245372, 
			'Sarah Edwards', 
			'1982-04-17', 
			'2916 SW Florence Place', 
			'Monmouth', 
			'OR', 
			97361, 
			'Polk',
			'2017-06-15'
	);
-- 3
INSERT INTO Requests (ODL, FullName, DOB, Street, City, USState, Zip, County, DateStamp)
	VALUES (3456789, 
			'Aiden Cross', 
			'2000-02-15', 
			'8456 SE Sceneca Street', 
			'Portland', 
			'OR', 
			97230, 
			'Multnomah',
			'2017-06-16'
	);
-- 4
INSERT INTO Requests (ODL, FullName, DOB, Street, City, USState, Zip, County, DateStamp)
	VALUES (9508231, 
			'Aithnea Stalenhag', 
			'1994-12-03', 
			'3126 NW Spruce Drive', 
			'Corvallis', 
			'OR', 
			97330, 
			'Benton',
			'2017-06-17'	
	);
-- 5
INSERT INTO Requests (ODL, FullName, DOB, Street, City, USState, Zip, County, DateStamp)
	VALUES (8703621, 
			'Andrea Sweet', 
			'1984-03-23', 
			'4782 W Spring Hills Dr', 
			'Wilsonville', 
			'OR', 
			97322, 
			'Clackamas',
			'2017-06-19'
	);
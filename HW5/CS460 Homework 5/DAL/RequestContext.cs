﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS460_Homework_5.Models;

namespace CS460_Homework_5.DAL
{
    /// <summary>
    /// Database context for the change of address request database.
    /// </summary>
    public class RequestContext : DbContext
    {
        public RequestContext() : base("name=ChangeRequestContext") { }
        public virtual DbSet<Request> Requests { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CS460_Homework_5.Models
{
    /// <summary>
    /// Request object for address change requests
    /// </summary>
    public class Request
    {
        /// <summary>
        /// The automatically generated ID in the database. This is the PRIMARY KEY.
        /// </summary>
        [Required]
        [Display (Name = "Request ID")]
        public int ID { get; set; }
        /// <summary>
        /// The Oregon Driver's License Number or Permit Number that needs to have the
        /// address change.
        /// </summary>
        [Required]
        [Display(Name = "ODL/Permit Number")]
        public int ODL { get; set; }
        /// <summary>
        /// The full legal name of the user requesting the address change.
        /// </summary>
        [Required]
        [StringLength(255)]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        /// <summary>
        /// The date of birth for the person requesting the address change.
        /// </summary>
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }
        /// <summary>
        /// The new street address the user is moving to
        /// </summary>
        [Required]
        [StringLength(255)]
        [Display(Name = "Street Address")]
        public string Street { get; set; }
        /// <summary>
        /// The city the user is moving to
        /// </summary>
        [Required]
        [StringLength(255)]
        [Display(Name = "City")]
        public string City { get; set; }
        /// <summary>
        /// The state that the user is moving to, in this case it should almost always be
        /// Oregon. This is only two characters for the state.
        /// </summary>
        [Required]
        [StringLength(2)]
        [Display(Name = "State")]
        public string USState { get; set; }
        /// <summary>
        /// The numeric postal code of the new address.
        /// </summary>
        [Required]
        [Display(Name = "Zip Code")]
        public int Zip { get; set; }
        /// <summary>
        /// The county in the state the new address resides in.
        /// </summary>
        [Required]
        [StringLength(100)]
        [Display(Name = "County")]
        public string County { get; set; }
        /// <summary>
        /// Today's current date. This should be the date of the request.
        /// </summary>
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Request Date")]
        public DateTime DateStamp { get; set; }


        /// <summary>
        /// Override for the ToString method that will output information for this model
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{base.ToString()}: {ODL} {FullName}  DOB = {DOB} Street = {Street} City = {City} State = {USState} Zip = {Zip} County = {County} ";
        }
    }

}
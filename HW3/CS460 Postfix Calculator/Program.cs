﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;



/*************************************************************************************************\
 * Postfix Calculator                                                                             *
 * Author: Devon Smith                                                                            *
 * CS360 Fall 2017                                                                                *
 * Western Oregon University                                                                      *
 \************************************************************************************************/

namespace CS460_Postfix_Calculator
{
    class Program
    {
        /// <summary>
        ///     Application calculates expressions in reverse polish notation.
        ///     Expressions are strings of operators and operands separated by
        ///     spaces.
        /// </summary>
        /// <param name="args">System arguments will be ignored.</param>

        // test value:
        //    infix: ((15 / (7 − (1 + 1))) * 3) − (2 + (1 + 1))
        //    postfix: 15 7 1 1 + − / 3 * 2 1 1 + + −
        //    Expected result: 5
        static void Main(string[] args)
        {
            // An indicator for application exit.
            bool calculateMore = true;
            //Provide instructions for the user
            Console.WriteLine("Postfix Calculator. Recognizes these operators: + - * / ");

            // If the user wants to do more calculations continue.
            while (calculateMore)
            {
                Console.Write("Please enter q to quit\n\n> ");
                string rawInput = Console.ReadLine();
                // split the input on a regular expression.
                string[] input = new Regex(@"\s+").Split(rawInput);
                // If the first character is not 
                if (input[0].FirstOrDefault() != 'q')
                {
                    // return the calculated value.
                    try
                    {
                        double result = Calculate(input);
                        Console.WriteLine("\t>>> " + result);
                    }
                    // If the calculation throws an exception.
                    catch (Exception e)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("\t>>> " + rawInput + " = ");
                        // If the user tried to divide by zero.
                        if (e is DivideByZeroException)
                        {
                            sb.Append("Can't divide by zero.");

                        }
                        // If the user provided an argument that was not recognized
                        else if (e is ArgumentException)
                        {
                            sb.Append("Improper input format. ");
                        }
                        // If the application could not convert one of the input values into a double.
                        else if (e is FormatException)
                        {
                            sb.Append("Improper input format. One or more values were invalid.");
                        }
                        // for all other exceptions.
                        // This is used when a user has an invalid stack state at expression end.
                        else
                        {
                            sb.Append(e.Message);
                        }
                        Console.WriteLine(sb.ToString());
                    }
                }
                else
                {
                    // Stop calculating.
                    calculateMore = false;
                }
               
            }
        }

        /// <summary>
        ///     Calculates the value of a reverse polish/postfix expression.
        /// </summary>
        /// <param name="arr">
        ///     An array that represents the tokenized expression to be
        ///     assessed.
        /// </param>
        /// <returns>
        ///     The double value solution of the provided expression.
        /// </returns>
        static double Calculate(string [] arr)
        {
            LinkedStack<double> calculatorStack = new LinkedStack<double>();
            foreach (string s in arr)
            {
                // if the value of the string is numeric.
                if (IsNumeric(s))
                    // push it onto the stack as a double.
                    calculatorStack.Push(Convert.ToDouble(s));
                // if the value of the string is an operator
                else if (IsOperator(s) && calculatorStack.Count > 1)
                {
                    // operands 1 and 2 for non-commutative operations.
                    double op1, op2;
                    // which operator is it?
                    switch (s)
                    {
                        // add
                        case "+":
                            calculatorStack.Push(calculatorStack.Pop() + calculatorStack.Pop());
                            break;
                        // subtract
                        case "-":
                            // non-commutative operation
                            op2 = calculatorStack.Pop();
                            op1 = calculatorStack.Pop();
                            calculatorStack.Push(op1 - op2);
                            break;
                        // multiply
                        case "*":
                            calculatorStack.Push(calculatorStack.Pop() * calculatorStack.Pop());
                            break;
                        // divide
                        case "/":
                            // non-commutative operation
                            op2 = calculatorStack.Pop();
                            op1 = calculatorStack.Pop();
                            // cannot divide by zero
                            if (op2 == 0)
                                throw new DivideByZeroException();
                            calculatorStack.Push(op1 / op2);
                            break;
                        // default, do nothing
                        default:
                            break;
                    }
                }
                // what if the value is something else?
                // die.
                else
                    // if the stack didn't have two or more items.
                    if (calculatorStack.Count <= 1)
                        throw new Exception("Improper input format. Stack became empty when expecting first operand");
                    // otherwise we encountered and invalid argument.
                    else
                        throw new ArgumentException();
            }
            // Return the value on the stack
            if (calculatorStack.Count > 1)
                // Throw an exception if the stack is not in a valid state at the end
                // of processing the expression.
                throw new Exception("Improper input format. Stack contains multiple elements.");
            return calculatorStack.Pop();
        }
        /// <summary>
        /// Returns true if a supplied string is a valid decimal number.
        /// </summary>
        /// <param name="num">The string to assess.</param>
        /// <returns>True if the string is a valid number.</returns>
        static bool IsNumeric(string num)
        {
            return new Regex(@"^[-|+]?(?:\d*.)?\d+$").Match(num).Success;
        }

        /// <summary>
        /// Returns if a supplied string is an operator for the calculator.
        /// </summary>
        /// <param name="input">the string to assess.</param>
        /// <returns>True if the value is +,-,*, or /</returns>
        static bool IsOperator(string input)
        {
            return new Regex(@"[-+*/]{1}").Match(input).Success;
        }
    }
}

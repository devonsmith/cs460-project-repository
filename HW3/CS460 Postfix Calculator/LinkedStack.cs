﻿using System;

namespace CS460_Postfix_Calculator
{

    class LinkedStack<T> : ILinkedStack<T>
    {
        // The top item on the stack
        Node<T> top;
        // The number of items on the stack.
        int count;
        /// <summary>
        /// Constructor for an empty stack.
        /// </summary>
        public LinkedStack()
        {
            top = null;
            count = 0;
        }

        /// <summary>
        /// Constructor that creates the first node in the stack.
        /// </summary>
        /// <param name="item">The item to be stored on the stack.</param>
        public LinkedStack(T item) {
            top = new Node<T>(item, null);
            count = 1;
        }

        /// <summary>
        /// Returns the top item from the stack. Removes the item from the stack.
        /// </summary>
        /// <returns></returns>
        public T Pop()
        {
            // If there is nothing on the stack, throw exception
            if (count == 0)
                throw new Exception("Empty Stack");
            T topItem =  top.Data;
            top = top.Next;
            --count;
            return topItem;
        }

        /// <summary>
        /// Returns the top item of the stack, does not remove it from the stack.
        /// </summary>
        public T Peek()
        {
            // If there is nothing on the stack, throw exception
            if (count == 0)
                throw new Exception("Empty Stack");
            return top.Data;
        }

        /// <summary>
        /// Push a new item onto the stack.
        /// </summary>
        public void Push(T newItem)
        {
            Node<T> node = new Node<T>(newItem, top);
            top = node;
            ++count;
        }

        /// <summary>
        /// Clears the stack of all elements.
        /// </summary>
        public void Clear()
        {
            top = null;
        }

        /// <summary>
        /// The number of elements in the stack.
        /// </summary>
        public int Count{
            get { return count; }
        }
    }
}

﻿
namespace CS460_Postfix_Calculator
{
    class Node<T>
    {
        T data;
        Node<T> next;
        /// <summary>
        /// Node code constructor, creates a new node with the items created.
        /// </summary>
        /// <param name="item">The item of type T that will be stored in the node.></param>
        /// <param name="nextNode">A pointer to the next node in the chain.</param>
        public Node(T item, Node<T> nextNode)
        {
            // The item we want to store
            data = item;
            // the next node in the linked list.
            next = nextNode;
        }
        // Properties for the node. In the Java version of this project they used 
        // public values. Since C# has properties we don't need to do that.
        public T Data
        {
            get { return data; }
            set { data = value; }
        }
        public Node<T> Next
        {
            get { return next; }
            set { next = value; }
        }

    }
}

﻿
namespace CS460_Postfix_Calculator
{
    interface ILinkedStack<T>
    {
        // the methods that the ILinkedStack classes must implement
        // Push items onto the stack.
        void Push(T obj);

        // Pop items off the stack
        T Pop();

        // Look at the top item on the stack.
        T Peek();

        // clear the contents of the stack.
        void Clear();

        // I excluded the isEmpty method that was included in the Java version of this project.
        // This method is not needed since C# uses properties. So I'm going to use a property, 
        // Count, for checking if the stack is empty.

    }
}

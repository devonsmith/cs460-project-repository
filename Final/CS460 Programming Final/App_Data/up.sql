﻿-- Buyers table
CREATE TABLE  Buyer(
	ID INT IDENTITY(0,1) PRIMARY KEY,
	BuyerName NVARCHAR(50) NOT NULL
);

-- Sellers table
CREATE TABLE Seller(
	ID INT IDENTITY(0,1) PRIMARY KEY,
 	SellerName NVARCHAR(50) NOT NULL
);

-- Item table
CREATE TABLE Item(
	ID INT IDENTITY(1001, 1) PRIMARY KEY,
	ItemName NVARCHAR(255) NOT NULL,
	ItemDescription NVARCHAR(255) NOT NULL,
	SellerID INT NOT NULL,
	CONSTRAINT fk_seller FOREIGN KEY (SellerID)
		REFERENCES Seller(ID)
);

-- Bid table
CREATE TABLE Bid(
	ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	ItemID INT NOT NULL,
	BuyerID INT NOT NULL,
	Price INT NOT NULL,
	BidTime DATETIME NOT NULL,
	CONSTRAINT fk_item FOREIGN KEY (ItemID) 
		REFERENCES Item(ID),
	CONSTRAINT fk_buyer FOREIGN KEY (BuyerID)
		REFERENCES Buyer(ID)
);

-- Entering Seed Data

-- Buyers
--0
INSERT INTO Buyer (BuyerName) VALUES ('Jane Stone');
--1
INSERT INTO Buyer (BuyerName) VALUES ('Tom McMasters');
--2
INSERT INTO Buyer (BuyerName) VALUES ('Otto Vanderwall');

-- Sellers
--0
INSERT INTO Seller (SellerName) VALUES ('Gale Hardy');
--1
INSERT INTO Seller (SellerName) VALUES('Lyle Banks');
--2
INSERT INTO Seller (SellerName) VALUES('Pearl Greene');

-- items
--1001
INSERT INTO Item (ItemName, ItemDescription, SellerID) VALUES ('Abraham Lincoln Hammer', 'A bench mallet fashioned from a broken rail-splitting maul in 1829 and owned by Abraham Lincoln', 2);
--1002
INSERT INTO Item (ItemName, ItemDescription, SellerID) VALUES ('Albert Einsteins Telescope', 'A brass telescope owned by Albert Einstein in Germany, circa 1927', 0);
--1003
INSERT INTO Item (ItemName, ItemDescription, SellerID) VALUES ('Bob Dylan Love Poems', 'Five version of an original unpublished, handwritten, love poem by Bob Dylan', 1);

--Bids
INSERT INTO Bid (ItemID, BuyerID, Price, BidTime) VALUES (1001, 2, 250000, '12/04/2017 09:042:2');
INSERT INTO Bid (ItemID, BuyerID, Price, BidTime) VALUES (1003, 0, 95000, '12/04/2017 08:44:03');

-- Testing Queries
SELECT * FROM Buyer;
SELECT * FROM Seller;
SELECT Item.ID, ItemName, ItemDescription, SellerName FROM Item JOIN Seller ON Item.SellerID = Seller.ID;
SELECT ItemName, BuyerName, Price, BidTime FROM (Bid JOIN Item ON Bid.ItemID = Item.ID) JOIN Buyer On Bid.BuyerID = Buyer.ID;


	
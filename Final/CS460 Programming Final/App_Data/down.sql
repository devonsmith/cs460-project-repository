﻿-- Drop all the tables in reverse order.
DROP TABLE Bid;
DROP TABLE Item;
DROP TABLE Seller;
DROP TABLE Buyer;
﻿using CS460_Programming_Final.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS460_Programming_Final.Models
{
    public class AuctionBid
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public string Time { get; set; }
    }
}
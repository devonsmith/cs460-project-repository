﻿// Get the Json item information from the server
function getBids(id) {
    $.ajax({
        type: "GET",
        url: "/Home/Bids/" + id,
        dataType: "json",
        success: function (data) { display(data); },
        error: function (data) { console.log("There was an error. Try again please!"); }
    });
}


// Display the data from the Json result.
function display(data) {
    console.log("get success!")
    $("#output").empty();
    var string = "<table class='table table- striped table-hover'>"
        + " <thead>"
        + "<th>Bidder</th>"
        + "<th>Price</th>"
        + "<th>BidTime</th>"
        + "</thead><tbody>"
    // For each item in the Json result, add the item to the table.
    $.each(data, function (i, item) {
        string = string
            + "<tr>"
            + "<td>"
            + item["Name"]
            + "</td>"
            + "<td>$"
            + item["Price"]
            + "</td>"
            + "<td>"
            + item["Time"]
            + "</td>"
            + "</tr>";
    });
    string += "</tbody><table>"
    $("#output").append(string);
}

﻿using CS460_Programming_Final.DAL;
using CS460_Programming_Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CS460_Programming_Final.Controllers
{
    public class HomeController : Controller
    {
        private AuctionContext db = new AuctionContext();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Bids(int id)
        {
            var bidList = db.Bids.Where(b => b.ItemID == id)
                .OrderByDescending(b => b.Price)
                .ToList();

            List<AuctionBid> bids = new List<AuctionBid>();
            foreach(var bid in bidList)
            {
                AuctionBid nb = new AuctionBid();
                nb.Name = bid.Buyer.BuyerName;
                nb.Price = bid.Price.ToString();
                nb.Time = bid.BidTime.ToString();
                bids.Add(nb);
            }
            return Json(bids, JsonRequestBehavior.AllowGet);
        }
    }

}
﻿CREATE TABLE Artist(
	ID INT IDENTITY(0,1) PRIMARY KEY,
	ArtistName NVARCHAR(255),
	BirthDate DATETIME2,
	BirthCity NVARCHAR(255)
);


CREATE TABLE Artwork(
	ID INT IDENTITY(0,1) PRIMARY KEY,
	Title NVARCHAR(255),
	ArtistID INT FOREIGN KEY REFERENCES Artist

);

CREATE TABLE Genre(
	ID INT IDENTITY(0,1) PRIMARY KEY,
	GenreName NVARCHAR(255) UNIQUE
);

CREATE TABLE Classification(
	ID INT IDENTITY(0,1) NOT NULL,
	ArtworkID INT FOREIGN KEY REFERENCES Artwork,
	GenreID INT FOREIGN KEY REFERENCES Genre,
	--PRIMARY KEY (ArtworkID, GenreID)
	CONSTRAINT [pk_dbo.Classifications] PRIMARY KEY CLUSTERED (ID ASC)
);



-- Artist
--0
INSERT INTO Artist(ArtistName, BirthDate, BirthCity) VALUES('M.C. Escher', '1898-6-17', 'Leeuwarden, Netherlands');
--1
INSERT INTO Artist(ArtistName, BirthDate, BirthCity) VALUES('Leonardo Da Vinci','1519-5-2','Vinci, Italy');
--2
INSERT INTO Artist(ArtistName, BirthDate, BirthCity) VALUES('Hatip Mehmed Efendi', '1680-11-18', 'Unknown');
--3
INSERT INTO Artist(ArtistName, BirthDate, BirthCity) VALUES('Salvador Dali', '1904-5-11', 'Figueres, Spain');

-- Artwork
--0
INSERT INTO Artwork (Title, ArtistID) VALUES('Circle Limit III', 0);
--1
INSERT INTO Artwork (Title, ArtistID) VALUES('Twon Tree', 0);
--2
INSERT INTO Artwork (Title, ArtistID) VALUES('Mona Lisa', 1);
--3
INSERT INTO Artwork (Title, ArtistID) VALUES('The Vitruvian Man', 1);
--4
INSERT INTO Artwork (Title, ArtistID) VALUES('Ebru', 2);
--5
INSERT INTO Artwork (Title, ArtistID) VALUES('Honey Is Sweeter Than Blood', 3);

-- Genre

--0
INSERT INTO Genre(GenreName) VALUES('Tesselation'); 
--1
INSERT INTO Genre(GenreName) VALUES('Surrealism');
--2
INSERT INTO Genre(GenreName) VALUES('Portrait');
--3
INSERT INTO Genre(GenreName) VALUES('Renaissance');


-- Classification
--0
INSERT INTO Classification (ArtworkID, GenreID) VALUES (0, 0);
--1
INSERT INTO Classification (ArtworkID, GenreID) VALUES (1, 0);
--2
INSERT INTO Classification (ArtworkID, GenreID) VALUES (1, 1);
--3
INSERT INTO Classification (ArtworkID, GenreID) VALUES (2, 2);
--4
INSERT INTO Classification (ArtworkID, GenreID) VALUES (2, 3);
--5
INSERT INTO Classification (ArtworkID, GenreID) VALUES (3, 3);
--6
INSERT INTO Classification (ArtworkID, GenreID) VALUES (4, 0);
--7
INSERT INTO Classification (ArtworkID, GenreID) VALUES (5, 1);


-- Testing Queries
SELECT * FROM Artist;
SELECT Title, ArtistName FROM Artwork JOIN Artist ON Artwork.ArtistID=Artist.ID;
SELECT * FROM Genre;
SELECT Title, GenreName 
	   FROM (Classification JOIN Artwork ON Classification.ArtworkID=Artwork.ID)
	   JOIN Genre ON Classification.GenreID=Genre.ID;


﻿function getGenre(id) {
    $.ajax({
        type: "GET",
        url: "/Home/Genre/" + id,
        dataType: "json",
        success: function (data) { display(data); },
        error: function (data) { alert("There was an error. Try again please!"); }
    });
}

function display(data) {
    $("#genreOutput").empty();
    var string = "<dl>"
    $.each(data, function (i, item) {
        string = string 
            + "<dt>"
            + item["artwork"]
            + "</dt>"
            + "<dd> by "
            + item["artist"]
            + "</dd>";
    });
    string += "</dl>"
    $("#genreOutput").append(string);
}

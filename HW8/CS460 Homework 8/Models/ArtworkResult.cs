﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS460_Homework_8.Models
{
    public class ArtworkResult
    {
        public string artist { get; set; }
        public string artwork { get; set; }
    }
}
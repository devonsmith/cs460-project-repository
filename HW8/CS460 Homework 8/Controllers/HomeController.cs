﻿using CS460_Homework_8.DAL;
using CS460_Homework_8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CS460_Homework_8.Controllers
{
    
    public class HomeController : Controller
    {
        // Database context to get the information from the database.
        ArtistsContext database = new ArtistsContext();

        // GET: Home
        public ActionResult Index()
        {
            
            return View(database.Genres.ToList());
        }

        // GET: Genre
        /// <summary>
        /// Returns a Json list of all the art with artist names that match requested
        /// genre ID.
        /// </summary>
        /// <param name="id">Genre ID</param>
        /// <returns></returns>
        public JsonResult Genre(int id)
        {
            // A list of ArtworkResuls objects we want to return to the index.
            List<ArtworkResult> list = new List<ArtworkResult>();
            // A query against the database context that will get the Artist Name and
            // the title of any piece that meets the GenreID requested.
            var artList = database.Genres.Where(g => g.ID == id)
                   .Select(s => s.Classifications)
                   .FirstOrDefault()
                   .OrderBy(o => o.Artwork.Title)
                   .ToList();

            // Go through the list of items we got from the database.
            foreach (var v in artList)
            {
                // create a new ArtworkResult that can be put in the Json result
                ArtworkResult piece = new ArtworkResult();
                // set the Properties of the piece to the values we want to pass to the Json result.
                piece.artist = v.Artwork.Artist.ArtistName;
                piece.artwork = v.Artwork.Title;
                // add the new piece to the list of artworks for the index.
                list.Add(piece);
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
namespace CS460_Homework_8.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Artist")]
    public partial class Artist
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Artist()
        {
            Artworks = new HashSet<Artwork>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Artist Name")]
        public string ArtistName { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        [Display (Name = "Birth Date")]
        [DateValidation] // Custom date validation
        public DateTime BirthDate { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Birth City")]
        public string BirthCity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Artwork> Artworks { get; set; }

        /// <summary>
        /// Custom date validation attribute to be used to validate the user birth date.
        /// </summary>
        public class DateValidationAttribute : ValidationAttribute
        {
            /// <summary>
            /// Takes the value in from the validation and returns true of false.
            /// </summary>
            /// <param name="value">The date entered by the user.</param>
            /// <returns></returns>
            public override bool IsValid(object value)
            {
                return (DateTime)value < DateTime.Now;
            }
        }
    }
}
